unit v.main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, AdvSpin, AdvEdit, VCLTee.TeCanvas, Vcl.ExtCtrls, System.Actions,
  Vcl.ActnList, Vcl.Buttons;

type
  TvMain = class(TForm)
    EditH: TAdvEdit;
    ButtonColor: TButtonColor;
    ActionList: TActionList;
    ActionNewForm: TAction;
    ButtonClose: TSpeedButton;
    ButtonRoate: TSpeedButton;
    ActionRotate: TAction;
    ActionClose: TAction;
    procedure ActionCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);

    procedure ActionNewFormExecute(Sender: TObject);
    procedure ActionRotateExecute(Sender: TObject);
    procedure EditHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ButtonColorClick(Sender: TObject);

  private
    FVertical: Boolean;
    procedure AlignCtrls;
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  Math, UITypes
  ;

function IfThen(const ACondition: Boolean; const ATrue, AFalse: TAlign): TAlign; overload;
begin
  if ACondition then
    Exit(ATrue)
  else
    Exit(AFalse);
end;

procedure TvMain.ActionCloseExecute(Sender: TObject);
begin
  if Application.MainForm = Self then
    if MessageDlg('프로그램을 종료 합니다.', mtInformation, [mbYes, mbNo], 0) = mrNo then
      Exit;

  Close;
end;

procedure TvMain.FormCreate(Sender: TObject);
begin
  Font.Assign(Application.DefaultFont);
  Font.Size := -10;
end;

procedure TvMain.ActionNewFormExecute(Sender: TObject);
var
  LForm: TvMain;
begin
  LForm := TvMain.Create(Self);
  LForm.FormStyle := fsStayOnTop;
  LForm.Height := Height;
  LForm.EditH.Text := Height.ToString;
  LForm.ButtonColor.SymbolColor := Color;
  LForm.ButtonColorClick(LForm.ButtonColor);
  LForm.Width := Width;
  LForm.Height := Height;
  LForm.FVertical := FVertical;
  LForm.Show;
  LForm.Left := Left + 8 + IfThen(FVertical, Width);
  LForm.Top := Top + 8 + IfThen(not FVertical, Height);
  if FVertical then
    LForm.AlignCtrls;
end;

procedure TvMain.ActionRotateExecute(Sender: TObject);
var
  h, w: Integer;
begin
  FVertical := not FVertical;
  w := Width;
  H := Height;
  Height := w;
  Width := h;
  AlignCtrls;
end;

procedure TvMain.EditHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    case FVertical of
      False: Height := EditH.Value;
      True: Width := EditH.Value;
    end
  else if Key = VK_UP then
    Top := Top -1
  else if key = vk_down then
    Top := Top + 1
  else if Key = VK_LEFT then
    Left := Left -1
  else if Key = VK_RIGHT then
    Left := Left +1;
end;

procedure TvMain.ButtonColorClick(Sender: TObject);
begin
  Color := ButtonColor.SymbolColor;
  EditH.Color := Color;
  EditH.FocusColor := Color;
  EditH.ModifiedColor := Color;
end;

procedure TvMain.AlignCtrls;
begin
  ButtonRoate.Align := IfThen(FVertical, alTop, alLeft);
  ButtonClose.Align := IfThen(FVertical, alTop, alLeft);
  ButtonClose.Top := 0;
  ButtonClose.Left := 0;
  ButtonColor.Align := IfThen(FVertical, alTop, alRight);
  EditH.Align := IfThen(FVertical, alTop, alRight);
end;

procedure TvMain.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_RESTORE = $F012;
begin
  if (Button = mbLeft) then
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_RESTORE, 0);
  end;
end;

procedure TvMain.FormShow(Sender: TObject);
begin
  if Application.MainForm = Self then
  begin
    ButtonClose.Caption := 'X';
    ButtonClose.Font.Style := [fsBold];
  end
  else
    ButtonClose.Font.Color := clWhite;

end;

end.
